#ifndef _USER_CONFIG_OVERRIDE_H_
#define _USER_CONFIG_OVERRIDE_H_

// force the compiler to show a warning to confirm that this file is included
#warning **** user_config_override.h: Using Settings from this File ****

// -- special hardware  ---------------
#ifdef CUSTOM_CONFIG_BATTERY  // *******************************************************************

#undef CODE_IMAGE_STR
#define CODE_IMAGE_STR "battery"

#define MODULE                 TUYA_DIMMER      // [Module] Defined TuyaMCU (54) as default

#define MQTT_CLEAN_SESSION   0                   // Mqtt clean session connection (0 = No clean session, 1 = Clean session (default))

#undef WIFI_SCAN_AT_RESTART                
#define WIFI_SCAN_AT_RESTART   false             // [SetOption56] Scan wifi network at restart for configured AP's

#undef WIFI_SCAN_REGULARLY    
#define WIFI_SCAN_REGULARLY    false             // [SetOption57] Scan wifi network every 44 minutes for configured AP's

#undef APP_DISABLE_POWERCYCLE
#define APP_DISABLE_POWERCYCLE true             // [SetOption65] Disable fast power cycle detection for device reset

#undef MQTT_TUYA_RECEIVED
#define MQTT_TUYA_RECEIVED     true             // [SetOption66] Enable TuyaMcuReceived messages over Mqtt

#undef KEY_DISABLE_MULTIPRESS 
#define KEY_DISABLE_MULTIPRESS true             // [SetOption1]  Disable button multipress

#undef BOOT_LOOP_OFFSET
#define BOOT_LOOP_OFFSET       0                 // [SetOption36] Number of boot loops before starting restoring defaults (0 = disable, 1..200 = boot loops offset)

#define USE_RULES                                // Add support for rules (+4k4 code)
#define USE_TUYA_MCU                             // Add support for Tuya Serial MCU
 #define USE_TUYA_TIME                           // Enable TuyaMCU sending local time to MCU

#define USE_DEEPSLEEP                            // Add support for deepsleep (+1k code)

#endif  // CUSTOM_CONFIG_BATTERY *******************************************************************

// -- Wifi settings  ---------------
#ifdef  STA_SSID1
#undef  STA_SSID1
#endif
#define STA_SSID1         "DEPLOY_IOT"             // [Ssid1] Wifi SSID

#ifdef  STA_PASS1
#undef  STA_PASS1
#endif
#define STA_PASS1         "insecure"               // [Password1] Wifi password

// -- MQTT settings  ---------------
#ifndef FIRMWARE_MINIMAL

#ifndef USE_MQTT_TLS
#define USE_MQTT_TLS
#endif

#ifndef USE_MQTT_TLS_CA_CERT
#define USE_MQTT_TLS_CA_CERT
#endif

#endif

#ifdef  MQTT_USE
#undef  MQTT_USE
#endif
#define MQTT_USE          true

#ifdef  MQTT_HOST
#undef  MQTT_HOST
#endif
#define MQTT_HOST         "mqtt.srvfarm.net"       // [MqttHost]

#ifdef  MQTT_PORT
#undef  MQTT_PORT
#endif
#define MQTT_PORT         8883                     // [MqttPort] MQTT port (10123 on CloudMQTT)

#ifdef  MQTT_USER
#undef  MQTT_USER
#endif
#define MQTT_USER         "m000000"                // [MqttUser] Optional user

#ifdef  MQTT_PASS
#undef  MQTT_PASS
#endif
#define MQTT_PASS         "m000000"                // [MqttPassword] Optional password

// -- OTA settings  ---------------
#ifdef  OTA_URL
#undef  OTA_URL
#endif
#define OTA_URL           "http://sh-updates.managed-infra.com/tasmota/tasmota.bin.gz"  // [OtaUrl]

// -- remove unneeded features  ---------------
#ifdef  USE_DOMOTICZ
#undef  USE_DOMOTICZ
#endif

#ifdef  USE_HOME_ASSISTANT
#undef  USE_HOME_ASSISTANT
#endif

#ifdef  USE_EMULATION_HUE
#undef  USE_EMULATION_HUE
#endif

#ifdef  USE_EMULATION_WEMO
#undef  USE_EMULATION_WEMO
#endif

#ifdef  USE_DISCOVERY
#undef  USE_DISCOVERY
#endif

#endif  // _USER_CONFIG_OVERRIDE_H_
