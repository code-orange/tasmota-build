#!/bin/bash
set -e
set +x

# Determine which version to base build on
CONF_UPSTREAM_GIT_TAG=`(cat buildconf/upstream_version)`

# Clone tasmota
rm -rf tasmota || true
git clone https://github.com/arendst/Tasmota.git tasmota

# Pull requested TAG
cd tasmota
git checkout $CONF_UPSTREAM_GIT_TAG

python3 -m venv venv
. venv/bin/activate
pip install --upgrade pip || true
pip install -r ../buildconf/requirements.txt || true

# Prepare conf
cp ../buildconf/user_config_override.h tasmota/user_config_override.h || true
cp ../buildconf/platformio_override.ini platformio_override.ini || true
cp ../buildconf/platformio_tasmota_cenv.ini platformio_tasmota_cenv.ini || true

# Run build
pio run
